colorscheme koehler
set number
startinsert
set mouse=a
set autoread
map <C-O> <ESC>:w<CR>
map <C-X> :q!<CR>
