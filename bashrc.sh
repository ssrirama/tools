# vd
function vd(){
  if [[ -x /usr/bin/touch ]] && [[ -x /bin/chown ]] && [[ -x /bin/chmod ]] && [[ -x /bin/ls ]] && [[ -x /bin/echo ]] && [[ -x /bin/pwd ]] && [[ -x /usr/bin/wc ]] && [[ -x /usr/bin/awk ]] && [[ -x /usr/bin/whoami ]] && [[ -x /usr/bin/dirname ]]
  then
    touch "$HOME"/.vd
    if [[ -f "$HOME"/.vd ]]
    then
      chown $USER:$USER "$HOME"/.vd
      if [[ $(ls -l "$HOME"/.vd | awk '{print $3}') == $(whoami) ]] && [[ $(ls -l "$HOME"/.vd | awk '{print $4}') == $(whoami) ]]
      then
        chmod 700 "$HOME"/.vd
        if [[ $(ls -l "$HOME"/.vd | awk '{print $1}') == "-rwx------" ]]
        then
          vdt
          # check how many arguments were supplied
          if [[ "$#" -eq 0 ]]
          then
            ls -FZalh
            echo $(($(ls -FZalh | wc -l)-3)) "items in" $(pwd) # three lines are extraneous
            #echo "$count items in" $(pwd)
          else
            pwd >> "$HOME"/.vd
            if [[ -f "$1" ]]
            then
              cd $(dirname "$@")
            else
              cd "$@"
            fi

            count=$(($(ls -FZalh | wc -l)-3)) # three lines are extraneous
            if [[ -z "$LINES" ]]
            then
              if [[ "$count" -ge 50 ]]
              then
                ls -a
              else
                ls -FZalh
              fi
            else
              if [[ "$count" -ge "$LINES" ]]
              then
                ls -a
              else
                ls -FZalh
              fi
            fi
            echo "$count items in" $(pwd)
            vdt
          fi
        fi
      fi
    fi
  fi
}

# vd Back
function vdb(){
  if [[ -x /usr/bin/touch ]] && [[ -x /bin/chown ]] && [[ -x /bin/chmod ]] && [[ -x /bin/ls ]] && [[ -x /bin/echo ]] && [[ -x /bin/pwd ]] && [[ -x /usr/bin/wc ]] && [[ -x /usr/bin/awk ]] && [[ -x /usr/bin/whoami ]] && [[ -x /usr/bin/dirname ]] && [[ -x /bin/sed ]] && [[ -x /usr/bin/shred ]] && [[ -x /bin/mv ]] && [[ -x /usr/bin/head ]] && [[ -x /usr/bin/tail ]]
  then
    touch "$HOME"/.vd
    if [[ -f "$HOME"/.vd ]]
    then
      chown $USER:$USER "$HOME"/.vd
      if [[ $(ls -l "$HOME"/.vd | awk '{print $3}') == $(whoami) ]] && [[ $(ls -l "$HOME"/.vd | awk '{print $4}') == $(whoami) ]]
      then
        chmod 700 "$HOME"/.vd
        if [[ $(ls -l "$HOME"/.vd | awk '{print $1}') == "-rwx------" ]]
        then
          vdt
          back=$(tail -1 "$HOME"/.vd)
          if [[ -n "$back" ]]
          then
            sed '$d' < "$HOME"/.vd > "$HOME"/.vd2
            shred -uz "$HOME"/.vd
            mv "$HOME"/.vd2 "$HOME"/.vd
            if [[ -f "$back" ]]
            then
              cd $(dirname "$back")
            else
              cd "$back"
            fi
            count=$(($(ls -FZalh | wc -l)-3)) # three lines are extraneous
            if [ -z "$LINES" ]
            then
              if [[ "$count" -ge 50 ]]
              then
                ls -a
              else
                ls -FZalh
              fi
            else
              if [[ "$count" -ge "$LINES" ]]
              then
                ls -a
              else
                ls -FZalh
              fi
            fi
            echo "$count" items in $(pwd)
            vdt
          else
            echo "No back available"
          fi
        else
          vdt
          echo "No back available"
        fi
      fi
    fi
  fi
}

# vd Trim
function vdt(){
  if [[ -x /bin/ls ]] && [[ -x /bin/cat ]] && [[ -x /usr/bin/touch ]] && [[ -x /bin/chown ]] && [[ -x /bin/chmod ]] && [[ -x /usr/bin/wc ]] && [[ -x /usr/bin/whoami ]]
  then
    touch "$HOME"/.vd
    if [[ -f "$HOME"/.vd ]]
    then
      chown $USER:$USER "$HOME"/.vd
      if [[ $(ls -l "$HOME"/.vd | awk '{print $3}') == $(whoami) ]] && [[ $(ls -l "$HOME"/.vd | awk '{print $4}') == $(whoami) ]]
      then
        chmod 700 "$HOME"/.vd
        if [[ $(ls -l "$HOME"/.vd | awk '{print $1}') == "-rwx------" ]]
        then
          while [[ $(cat "$HOME"/.vd | wc -l) -ge 11 ]]
          do
            tail -n +2 "$HOME"/.vd > "$HOME"/.vd
          done
        fi
      fi
    fi
  fi
}

#vd clear
function vdc(){
  clear &&  vd "$@"
}

function decrypt(){
  if [[ "$#" != 1 ]] || [[ ! -f "$1" ]]
  then
    echo "Usage:"
    echo "      decrypt <filePath>"
  else
    gpg --output "$1.decrypted" --decrypt "$1"
  fi
}

function encrypt(){
  if [[ "$#" -ne 2 ]] || [[ ! -f "$1" ]]
  then
    echo "Usage:"
    echo "      encrypt <filePath> <GPGrecipient>"
    echo ""
    echo "Example:"
    echo "      encrypt ../file1.txt timetoplatypus@protonmail.com"
  else
    gpg --output "$1.encrypted" --recipient "$2" --encrypt "$1"
  fi
}

function encryptText(){
  if [[ "$#" != 2 ]] || [[ ! -f "$1" ]]
  then
    echo "Usage:"
    echo "      encryptText <filePath> <GPGrecipient>"
    echo ""
    echo "Example:"
    echo "      encryptText ~/file.txt timetoplatypus@protonmail.com"
  else
    gpg --output "$1.encrypted" --armor --recipient "$2" --encrypt "$1"
    cat $1.encrypted
  fi
}

function matrix(){
  cmatrix -a -b -u 8
}

function rms(){
  if [[ "$#" -le 0 ]]
  then
    echo "Usage:"
    echo "  rms [FILE PATH]"
    echo "  rms [DIRECTORY PATH]"
  else
    for o in "$@"
    do
      if [[ -f "$o" ]]
      then
        shred -vuz "$f"
      elif [[ -d "$o" ]]
      then
        find "$o" -type f -exec shred -vuz {} \;
        rm -vrf "$o"
      else
        echo "Skipping $o. Not a file or directory"
      fi
    done
  fi
}
